/*******************************************************************************
 * Copyright 2014 Isos Technology, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.isostech.atlassian.confluence.okta;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ExternalApplication implements Serializable {
	private static final long serialVersionUID = 3807358875538059605L;

	@XmlElement private String applicationId;
	@XmlElement private boolean oktaApplication;
	@XmlElement private boolean enabled;
	@XmlElement private String label;
	@XmlElement private String applicationLink;
	@XmlElement private String applicationLogo;
	
	public ExternalApplication() {
		
	}
	
	public ExternalApplication(	String applicationId, boolean oktaApplication, boolean enabled, String label,
			String applicationLink, String applicationLogo ) {
		this.applicationId = applicationId;
		this.oktaApplication = oktaApplication;
		this.enabled = enabled;
		this.label = label;
		this.applicationLink = applicationLink;
		this.applicationLogo = applicationLogo;
	}
	
	
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public boolean isOktaApplication() {
		return oktaApplication;
	}
	public void setOktaApplication(boolean oktaApplication) {
		this.oktaApplication = oktaApplication;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getApplicationLink() {
		return applicationLink;
	}
	public void setApplicationLink(String applicationLink) {
		this.applicationLink = applicationLink;
	}
	public String getApplicationLogo() {
		return applicationLogo;
	}
	public void setApplicationLogo(String applicationLogo) {
		this.applicationLogo = applicationLogo;
	}

}
