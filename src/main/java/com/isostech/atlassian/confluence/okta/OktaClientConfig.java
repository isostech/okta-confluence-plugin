/*******************************************************************************
 * Copyright 2014 Isos Technology, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.isostech.atlassian.confluence.okta;

import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
/**
 * OktaClient Configuration object to hold the okta host, okta api token and the application configuration?
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class OktaClientConfig {
	@XmlElement private String oktaHost;
	@XmlElement private String oktaApiToken;
	
	@XmlElement private Collection<ExternalApplication> applications;
	
	public String getOktaHost() {
		return oktaHost;
	}
	public void setOktaHost(String oktaHost) {
		this.oktaHost = oktaHost;
	}
	public String getOktaApiToken() {
		return oktaApiToken;
	}
	public void setOktaApiToken(String oktaApiToken) {
		this.oktaApiToken = oktaApiToken;
	}
	public Collection<ExternalApplication> getApplications() {
		if (applications == null) {
			applications = new ArrayList<ExternalApplication>();
		}
		return applications;
	}
	public void setApplications(Collection<ExternalApplication> applications) {
		this.applications = applications;
	}
	public void addApplication(ExternalApplication application) {
		getApplications().add(application);
	}
}
