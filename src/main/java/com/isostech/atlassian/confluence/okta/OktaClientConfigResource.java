/*******************************************************************************
 * Copyright 2014 Isos Technology, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.isostech.atlassian.confluence.okta;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.lang.StringUtils;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.isostech.okta.client.OktaApiClient;
import com.isostech.okta.client.OktaApiClientImpl;
import com.isostech.okta.client.exceptions.OktaApiClientException;
/**
 * Rest implementation to read and save the OktaClientConfig in JSON form
 */
@Path("oktaclient-config")
public class OktaClientConfigResource {

	private final UserManager userManager;
	private final OktaConfigDao oktaConfigDao;

	public OktaClientConfigResource(UserManager userManager, OktaConfigDao oktaConfigDao) 
	{
		this.userManager = userManager;
		this.oktaConfigDao = oktaConfigDao;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@Context HttpServletRequest request) {
		if (!isAdminUser(request)) {
			return Response.status(Status.UNAUTHORIZED).build();
		}
		return Response.ok(oktaConfigDao.readOktaClientConfig()).build();
	}
	 
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response put(final OktaClientConfig config, @Context HttpServletRequest request) {
		if (!isAdminUser(request)) {
			return Response.status(Status.UNAUTHORIZED).build();
		} else {
			URI host = validateOktaHostData(config);
			if (host == null) {
				return Response.status(Status.BAD_REQUEST).entity(new OktaError("oktaHost", "InvalidUrl")).build();
			}
			String error = validateOktaApiToken(config);
			if (StringUtils.isNotBlank(error)){
				return Response.status(Status.BAD_REQUEST).entity(new OktaError("oktaApiToken", error)).build();
			}
			oktaConfigDao.saveOktaClientConfig(config);
		}
		return Response.noContent().build();
	}	
	
	private boolean isAdminUser(HttpServletRequest request) {
		UserProfile user = userManager.getRemoteUser(request);
		return user != null && userManager.isSystemAdmin(user.getUserKey());	
	}
	
	private URI validateOktaHostData(OktaClientConfig config) {
		try {
			URI uri = new URI(config.getOktaHost());
			if (uri.getHost() == null) {
				return null;
			};
			return uri;
		} catch (URISyntaxException ui) {
			return null; 
		}
	}
	private String validateOktaApiToken(OktaClientConfig config) {
		OktaApiClient oktaClient = new OktaApiClientImpl();
		oktaClient.setBaseOktaApiUrl(config.getOktaHost());
		oktaClient.setOktaApiToken(config.getOktaApiToken());
		oktaClient.init();
		try {
			oktaClient.listAllApplications();
			return null;
		} catch (OktaApiClientException oe) {
			return oe.getMessage();
		}		
	}
	
}
