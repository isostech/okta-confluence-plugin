/*******************************************************************************
 * Copyright 2014 Isos Technology, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.isostech.atlassian.confluence.okta;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Collection;

import org.apache.commons.lang.StringUtils;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class OktaConfigDao {

	private final PluginSettingsFactory pluginSettingsFactory;
	private final TransactionTemplate transactionTemplate;

	public OktaConfigDao(PluginSettingsFactory pluginSettingsFactory, TransactionTemplate transactionTemplate) {
		this.pluginSettingsFactory = pluginSettingsFactory;
		this.transactionTemplate = transactionTemplate;
	}

	public void saveOktaClientConfig(final OktaClientConfig config) {
		transactionTemplate.execute(new TransactionCallback<OktaClientConfig>() {
			public OktaClientConfig doInTransaction() {
				PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
				pluginSettings.put(OktaClientConfig.class.getName() + ".oktaHost", config.getOktaHost());
				pluginSettings.put(OktaClientConfig.class.getName() + ".oktaApiToken", config.getOktaApiToken());
				writeApplications(pluginSettings, config);
				return null;
			}
		});

	}

	public OktaClientConfig readOktaClientConfig() {
		return transactionTemplate.execute(new TransactionCallback<OktaClientConfig>() {
			public OktaClientConfig doInTransaction() {
				PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
				OktaClientConfig config = new OktaClientConfig();
				config.setOktaHost((String) settings.get(OktaClientConfig.class.getName() + ".oktaHost"));
				config.setOktaApiToken((String) settings.get(OktaClientConfig.class.getName() + ".oktaApiToken"));
				readApplications(settings, config);
				return config;
			}
		});
	}

	private void readApplications(PluginSettings settings, OktaClientConfig config) {
		try {
			String jsonApps = (String) settings.get(OktaClientConfig.class.getName() + ".extenalsApplications");
			if (StringUtils.isNotBlank(jsonApps)) {
				JsonReader reader = new JsonReader(new StringReader(jsonApps));
				reader.beginArray();
				while (reader.hasNext()) {
					config.addApplication(readApplication(reader));
				}
				reader.endArray();
			}
		} catch (IOException io) {
			io.printStackTrace();

		}
	}

	private ExternalApplication readApplication(JsonReader reader) throws IOException {
		ExternalApplication app = new ExternalApplication();
		reader.beginObject();
		while (reader.hasNext()) {
			String name = reader.nextName();
			if (name.equals("applicationId")) {
				app.setApplicationId(reader.nextString());
			} else if (name.equals("oktaApplication")) {
				app.setOktaApplication(reader.nextBoolean());
			} else if (name.equals("enabled")) {
				app.setEnabled(reader.nextBoolean());
			} else if (name.equals("label")) {
				app.setLabel(reader.nextString());
			} else if (name.equals("applicationLink")) {
				app.setApplicationLink(reader.nextString());
			} else if (name.equals("applicationLogo")) {
				app.setApplicationLogo(reader.nextString());
			}
		}
		reader.endObject();
		return app;
	}

	private void writeApplications(PluginSettings settings, OktaClientConfig config) {
		try {
			StringWriter strWriter = new StringWriter();
			JsonWriter writer = new JsonWriter(strWriter);
			writer.beginArray();
			for (ExternalApplication app : config.getApplications()) {
				writeApplication(writer, app);
			}
			writer.endArray();
			settings.put(OktaClientConfig.class.getName() + ".extenalsApplications", strWriter.toString());
		} catch (IOException io) {
			io.printStackTrace();
		}
	}

	private void writeApplication(JsonWriter writer, ExternalApplication application) throws IOException {
		writer.beginObject();
		writer.name("applicationId").value(application.getApplicationId());
		writer.name("oktaApplication").value(application.isOktaApplication());
		writer.name("enabled").value(application.isEnabled());
		writer.name("label").value(application.getLabel());
		writer.name("applicationLink").value(application.getApplicationLink());
		writer.name("applicationLogo").value(application.getApplicationLogo());
		writer.endObject();
	}

	public Collection<ExternalApplication> getAllApplications() {
		return transactionTemplate.execute(new TransactionCallback<Collection<ExternalApplication>>() {
			public Collection<ExternalApplication> doInTransaction() {
				PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
				OktaClientConfig config = new OktaClientConfig();
				config.setOktaHost((String) settings.get(OktaClientConfig.class.getName() + ".oktaHost"));
				config.setOktaApiToken((String) settings.get(OktaClientConfig.class.getName() + ".oktaApiToken"));
				readApplications(settings, config);
				return config.getApplications();
			}
		});
	}

}
