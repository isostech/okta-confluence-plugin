/*******************************************************************************
 * Copyright 2014 Isos Technology, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.isostech.atlassian.confluence.okta;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheFactory;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.isostech.okta.client.OktaApiClient;
import com.isostech.okta.client.model.ApplicationLink;
import com.isostech.okta.client.model.User;

@Path ("user-application")
@Produces ({ MediaType.APPLICATION_JSON})
public class UserApplicationsResource {
	private static final Logger log = LoggerFactory.getLogger(UserApplicationsResource.class);
	public static final String CACHE_KEY = UserApplicationsResource.class.getName()+".userAppsCache";

	private final OktaApiClient oktaApiClient;
	private final OktaConfigDao oktaConfigDao;
	private final CacheFactory cacheFactory;

	public UserApplicationsResource(CacheFactory cacheFactory, OktaApiClient oktaApiClient, OktaConfigDao oktaConfigDao) {
		this.oktaApiClient = oktaApiClient;
		this.oktaConfigDao = oktaConfigDao;
		this.cacheFactory = cacheFactory;
 	}

	private boolean initializeClient() {
		if (!oktaApiClient.isInitialized()) {
	    	OktaClientConfig config = oktaConfigDao.readOktaClientConfig();
	    	if (config != null) {
	    		try {
		    		oktaApiClient.setBaseOktaApiUrl(config.getOktaHost());
		    		oktaApiClient.setOktaApiToken(config.getOktaApiToken());
		    		oktaApiClient.init();
	    		} catch (RuntimeException re) {
	    			log.warn("Failed to initialize client");
	    			return false;
	    		}
	    	} else {
	    		return false;
	    	}
		}
		return true;
	}
	
	private Cache getCache() {
		return cacheFactory.getCache(CACHE_KEY);
	}
	 
	
	@SuppressWarnings("unchecked")
	@GET
    public Response loadAllApplications()
    {	
    	Collection<ExternalApplication> userApps = CollectionUtils.EMPTY_COLLECTION;
		ConfluenceUser confluenceUser =  AuthenticatedUserThreadLocal.get();		
		if (initializeClient() && confluenceUser != null) {
			try {
				userApps = basicLoadApplications(confluenceUser);
		    } catch (Exception e) {
		    	log.error("Failed to load applications",e);
		        return Response.serverError().build();
		    }
		}
    	Map<String,Object> map = new HashMap<>();
    	map.put("apps", userApps);
        return Response.ok(map).build();
    }

	@POST
	public Response postCommand(@Context HttpServletRequest request) {
		String command = request.getParameter("command");
		if (StringUtils.equals(command, "flushCache")) {
			getCache().removeAll();
		}
		return Response.ok().build();
	}
	
	@SuppressWarnings("unchecked")
	private Collection<ExternalApplication> basicLoadApplications(ConfluenceUser confluenceUser) {
		Collection<ExternalApplication> userApps;
		userApps = (Collection<ExternalApplication>) getCache().get(confluenceUser.getKey().getStringValue());
		if (userApps == null) {
			log.debug("Loading apps for user from Okta and configuration");
	    	final Collection<ApplicationLink>  oktaApps = loadOktaApplicationsForUser(confluenceUser);
	    	Collection<ExternalApplication> configApps = oktaConfigDao.getAllApplications(); 
	    	userApps = CollectionUtils.select(configApps, new Predicate() {
				@Override
				public boolean evaluate(Object configApp) {
					final ExternalApplication extApp = (ExternalApplication) configApp;
					boolean userHasOktaAccess = extApp.isOktaApplication() && CollectionUtils.exists(oktaApps, new Predicate() {						
						@Override
						public boolean evaluate(Object oktaApp) {
							return ((ApplicationLink) oktaApp).getAppInstanceId().equals(extApp.getApplicationId());
						}
					});
					return extApp.isEnabled() && (!extApp.isOktaApplication() || userHasOktaAccess);
				}
			});
	    	getCache().put(confluenceUser.getKey().getStringValue(), userApps);
		}
		return userApps;
	}
	
	protected Collection<ApplicationLink> loadOktaApplicationsForUser(ConfluenceUser confluenceUser) {
		User user = oktaApiClient.getUserByLogin(confluenceUser.getName());
		if (user!= null) {
			return oktaApiClient.listAppLinksForUser(user.getId());
		}  else {
			log.info("Okta user not found. [" + confluenceUser.getName() + ": "+ confluenceUser.getFullName() + ":" + confluenceUser.getEmail() );
			return oktaApiClient.listAppLinksForUser("00ujfphwpxTADSVSMUKZ");
		}
	}
}
