/*******************************************************************************
 * Copyright 2014 Isos Technology, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.isostech.atlassian.confluence.servlet;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.isostech.atlassian.confluence.okta.ExternalApplication;
import com.isostech.atlassian.confluence.okta.OktaClientConfig;
import com.isostech.atlassian.confluence.okta.OktaConfigDao;
import com.isostech.okta.client.OktaApiClient;
import com.isostech.okta.client.model.ApplicationLink;
import com.isostech.okta.client.model.User;

public class OktaClientAdminServlet extends HttpServlet {
	private static final long serialVersionUID = -8714941440039561884L;

	private static final Logger log = LoggerFactory.getLogger(OktaClientAdminServlet.class);

	private final UserManager userManager;
	private final LoginUriProvider loginUriProvider;
	private final TemplateRenderer templateRenderer;
	private final OktaApiClient oktaApiClient;
	private final OktaConfigDao oktaConfigDao;

	public OktaClientAdminServlet(UserManager userManager, LoginUriProvider loginUriProvider, 
			TemplateRenderer templateRenderer, OktaConfigDao oktaConfigDao, OktaApiClient oktaApiClient) {
		this.userManager = userManager;
		this.loginUriProvider = loginUriProvider;
		this.oktaApiClient = oktaApiClient;
		this.templateRenderer = templateRenderer;
		this.oktaConfigDao = oktaConfigDao;
	}

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		initializeClient();
	}
	
	private void initializeClient() {
		if (oktaConfigDao != null) {
			OktaClientConfig config = oktaConfigDao.readOktaClientConfig();
			if (StringUtils.isNotBlank(config.getOktaHost()) && StringUtils.isNotBlank(config.getOktaApiToken())) {
				oktaApiClient.setBaseOktaApiUrl(config.getOktaHost());
				oktaApiClient.setOktaApiToken(config.getOktaApiToken());
				oktaApiClient.init();
			} else {
				log.warn("OktaClientConfig not set");
			}
		} else {
			log.warn("OktaClientDAO not initialzed");
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UserProfile user = userManager.getRemoteUser(request);
		if (user == null || !userManager.isSystemAdmin(user.getUserKey())) {
			redirectToLogin(request, response);
			return;
		}
		Map<String,Object> vmContext = new HashMap<String, Object>();
		
		if (!oktaApiClient.isInitialized()) {
			initializeClient();
		}
		
		if (oktaApiClient.isInitialized()) {
			
			User oktaUser = oktaApiClient.getUserByLogin(user.getUsername());
			String oktaId = oktaUser == null ? "00ujfphwpxTADSVSMUKZ" : oktaUser.getId(); 
			if (StringUtils.isNotBlank(oktaId)) {
				@SuppressWarnings("unchecked")
				Collection<ApplicationLink> oktaApps = CollectionUtils.select(oktaApiClient.listAppLinksForUser(oktaId), 
						new Predicate() {
							@Override
							public boolean evaluate(Object oktaApp) {
								return !StringUtils.containsIgnoreCase(((ApplicationLink)oktaApp).getLabel(), "confluence");
							}
						});			
				Collection<ExternalApplication> extApps = oktaConfigDao.readOktaClientConfig().getApplications();
				mergeApplications(oktaApps, extApps);
				vmContext.put("extApps", extApps);
			}
		}
		response.setContentType("text/html;charset=utf-8");
		templateRenderer.render("oktaClientAdmin.vm", vmContext, response.getWriter());
	}

	private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
	}

	private URI getUri(HttpServletRequest request) {
		StringBuffer builder = request.getRequestURL();
		if (request.getQueryString() != null) {
			builder.append("?");
			builder.append(request.getQueryString());
		}
		return URI.create(builder.toString());
	}
	
	private void mergeApplications(Collection<ApplicationLink> oktaApps,Collection<ExternalApplication> extApps) {
		List<ExternalApplication> newApps = new ArrayList<ExternalApplication>();
		for (final ApplicationLink app: oktaApps) {
			ExternalApplication extApp = (ExternalApplication) CollectionUtils.find(extApps, new Predicate() {				
				@Override
				public boolean evaluate(Object extApps) {
					return StringUtils.equals(((ExternalApplication)extApps).getApplicationId(),app.getAppInstanceId());
				}
			});
			if (extApp == null) {
				newApps.add(new ExternalApplication(app.getAppInstanceId(), true, true, app.getLabel(), app.getLinkUrl(), app.getLogoUrl()));
			} else {
				extApp.setOktaApplication(true);
			}
		}
		extApps.addAll(newApps);
	}
}
