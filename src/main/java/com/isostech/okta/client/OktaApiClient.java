/*******************************************************************************
 * Copyright 2014 Isos Technology, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.isostech.okta.client;

import java.util.Collection;

import com.isostech.okta.client.model.Application;
import com.isostech.okta.client.model.ApplicationLink;
import com.isostech.okta.client.model.User;

public interface OktaApiClient {
	void init();
	void setBaseOktaApiUrl(String oktaHost);
	void setOktaApiToken(String oktaApiToken);
	User getUserByLogin(String login);
	Collection<ApplicationLink> listAppLinksForUser(String userId);
	Collection<Application> listAllApplications();
	boolean isInitialized();

}
