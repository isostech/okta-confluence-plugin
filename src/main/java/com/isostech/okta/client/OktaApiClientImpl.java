/*******************************************************************************
 * Copyright 2014 Isos Technology, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.isostech.okta.client;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.PostConstruct;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.isostech.okta.client.exceptions.OktaApiClientException;
import com.isostech.okta.client.model.Application;
import com.isostech.okta.client.model.ApplicationLink;
import com.isostech.okta.client.model.ErrorInfo;
import com.isostech.okta.client.model.User;

public class OktaApiClientImpl implements OktaApiClient {
	private static final String APP_URL_BASE = "/api/v1/apps";
	private static final String USER_URL_BASE = "/api/v1/users";
	
	
	private HttpClient httpClient;
	private URI oktaHost;
	private String oktaApiToken;
	
	public void setHttpClient(HttpClient httpClient) {
		this.httpClient = httpClient;
	}
	
	@Override
	public void setBaseOktaApiUrl(String oktaHost) {
		try {
			this.oktaHost = new URI(oktaHost);
		} catch (URISyntaxException ex)  {
			throw new IllegalArgumentException("OKTA Host String " + oktaHost + " is invalid");
		}
	}

	@Override
	public void setOktaApiToken(String oktaApiToken) {
		this.oktaApiToken = oktaApiToken;
	}

	@PostConstruct
	public void init() {
		httpClient = new HttpClient();
	}
	
	@Override
	public boolean isInitialized() {
		return httpClient != null && oktaHost != null & StringUtils.isNotBlank(oktaApiToken);
	}
	
	@Override
	public User getUserByLogin(String login) {
		try {
			URI listUserUri = new URI(oktaHost.getScheme(), oktaHost.getHost(), USER_URL_BASE + "/" + login,"");		
			String response  = handlerGetRequest(listUserUri);
			if (StringUtils.isNotBlank(response)) {
				return getObjectMapper().readValue(response, User.class);
			} else { 
				return null;
			}
		} catch (URISyntaxException ex) {
			throw new IllegalArgumentException("Application base URL is incorrect " + APP_URL_BASE);
		} catch (IOException e) {
			throw new OktaApiClientException("Failed to read response from listAppLinksForUser ", e);
		}		
	}
	
	@Override
	public Collection<ApplicationLink> listAppLinksForUser(String userId) {
		try {
			URI listUserUri = new URI(oktaHost.getScheme(), oktaHost.getHost(), USER_URL_BASE + "/" + userId + "/appLinks","");		
			String response = handlerGetRequest(listUserUri);
			if (StringUtils.isNotBlank(response)) {
				return getObjectMapper().readValue(response, new TypeReference<Collection<ApplicationLink>>() {});
			} else { 
				return new ArrayList<ApplicationLink>();
			}
		} catch (URISyntaxException ex) {
			throw new IllegalArgumentException("Application base URL is incorrect " + APP_URL_BASE);
		} catch (IOException e) {
			throw new OktaApiClientException("Failed to read response from listAppLinksForUser ", e);
		}		
	}
	
	@Override
	public Collection<Application> listAllApplications() {
		try {
			URI listAppsUri = new URI(oktaHost.getScheme(), oktaHost.getHost(), APP_URL_BASE, "");
			String response = handlerGetRequest(listAppsUri);
			if (StringUtils.isNotBlank(response)) {
				return getObjectMapper().readValue(response, new TypeReference<Collection<Application>>() {});
			} else { 
				return new ArrayList<Application>();
			}
		} catch (URISyntaxException ex) {
			throw new IllegalArgumentException("Application base URL is incorrect " + APP_URL_BASE);
		} catch (IOException jse) {
			throw new OktaApiClientException("Failed to read response from listAllApplications ", jse);			
		}
	}
	
	private String handlerGetRequest(URI listAppsUri) {
		HttpMethod get = createRequest(listAppsUri);
		 
		try {
			int statusCode = httpClient.executeMethod(get);
			switch (statusCode) { 
				case HttpStatus.SC_OK:{
					return IOUtils.toString(get.getResponseBodyAsStream());
				}
				case HttpStatus.SC_UNAUTHORIZED: {
					ErrorInfo errorInfo = getObjectMapper().readValue(get.getResponseBodyAsStream(), ErrorInfo.class);
					throw new OktaApiClientException(errorInfo.getErrorSummary());
				}
				case HttpStatus.SC_NOT_FOUND: {
					return null;
				}
				default: {
					String responseText = IOUtils.toString(get.getResponseBodyAsStream());
					throw new OktaApiClientException("Unhandled status code " + statusCode + ". " + responseText);
				}
			}
		} catch (IOException e) {
			throw new OktaApiClientException("Failed to read response from listAllApplications ", e);
		} finally {
			get.releaseConnection();
		}
	}

	protected ObjectMapper getObjectMapper() {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return objectMapper;
	}
	
	protected HttpMethod createRequest(URI uri) {
		GetMethod request = new GetMethod(uri.toString());
		request.addRequestHeader("Authorization", "SSWS " + oktaApiToken);
		request.addRequestHeader("Accept", "application/json");
		request.addRequestHeader("Content-Type","application/json");
		return request;
	}
}


