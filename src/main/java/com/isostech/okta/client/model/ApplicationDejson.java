/*******************************************************************************
 * Copyright 2014 Isos Technology, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.isostech.okta.client.model;

import java.io.IOException;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.ObjectCodec;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;

public class ApplicationDejson extends JsonDeserializer<Application> {

	@Override
	public Application deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		Application application = new Application();
		
		ObjectCodec oc = jsonParser.getCodec();
        JsonNode node = oc.readTree(jsonParser);
        
		application.setId(node.get("id").getTextValue());
		application.setName(node.get("name").getTextValue());
		application.setLabel(node.get("label").getTextValue());
		application.setStatus(node.get("status").getTextValue());
		JsonNode appLinks =  node.get("_links").get("appLinks");
		if (appLinks != null && appLinks.isArray()) {
			JsonNode firstLink = appLinks.iterator().next();
			application.setLinkUrl(firstLink.get("href").getTextValue());
		}
//		application.setCreated(node.get("created").get);
//		
//		application.setName(name);
//		application.setName(name);
		
		return application;
	}

}

