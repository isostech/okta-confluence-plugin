/*******************************************************************************
 * Copyright 2014 Isos Technology, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.isostech.okta.client.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ApplicationLink implements Serializable {
	private static final long serialVersionUID = -17191278696275073L;   
	
	@XmlElement
	private String label;
    @XmlElement
	private String linkUrl;
    @XmlElement
	private String logoUrl;
    @XmlElement
	private String appName;
    @XmlElement
	private String appInstanceId;
    @XmlElement
	private String appAssignmentId;
    @XmlElement
	private String credentialsSetup;
    @XmlElement
	private String hidden;
    @XmlElement
	private String sortOrder;

	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getLinkUrl() {
		return linkUrl;
	}
	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}
	public String getLogoUrl() {
		return logoUrl;
	}
	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	public String getAppInstanceId() {
		return appInstanceId;
	}
	public void setAppInstanceId(String appInstanceId) {
		this.appInstanceId = appInstanceId;
	}
	public String getAppAssignmentId() {
		return appAssignmentId;
	}
	public void setAppAssignmentId(String appAssignmentId) {
		this.appAssignmentId = appAssignmentId;
	}
	public String getCredentialsSetup() {
		return credentialsSetup;
	}
	public void setCredentialsSetup(String credentialsSetup) {
		this.credentialsSetup = credentialsSetup;
	}
	public String getHidden() {
		return hidden;
	}
	public void setHidden(String hidden) {
		this.hidden = hidden;
	}
	
	public String getSortOrder() {
		return sortOrder;
	}
	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}
	
}
