/*******************************************************************************
 * Copyright 2014 Isos Technology, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package com.isostech.okta.client.model;

import java.io.Serializable;

public class ErrorInfo implements Serializable {
	private static final long serialVersionUID = 9207208698690485481L;

	private String errorCode;
	private String errorSummary;
	private String errorLink;
	private String errorId;
	private String[] errorCauses;
	
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorSummary() {
		return errorSummary;
	}
	public void setErrorSummary(String errorSummary) {
		this.errorSummary = errorSummary;
	}
	public String getErrorLink() {
		return errorLink;
	}
	public void setErrorLink(String errorLink) {
		this.errorLink = errorLink;
	}
	public String getErrorId() {
		return errorId;
	}
	public void setErrorId(String errorId) {
		this.errorId = errorId;
	}
	public String[] getErrorCauses() {
		return errorCauses;
	}
	public void setErrorCauses(String[] errorCauses) {
		this.errorCauses = errorCauses;
	}
	
	
}
