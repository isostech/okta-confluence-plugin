/*******************************************************************************
 * Copyright 2014 Isos Technology, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

AJS.toInit(function() {
  var baseUrl = AJS.$("meta[name='application-base-url']").attr("content");
     
  function populateForm() {
    AJS.$.ajax({
      url: AJS.contextPath() + "/rest/oktaclient-config/1.0/oktaclient-config",
      dataType: "json",
      success: function(config) {
        AJS.$("#oktaHost").attr("value", config.oktaHost);
        AJS.$("#oktaApiToken").attr("value", config.oktaApiToken);
      }
    });
  
  }
  function updateConfig() {
	  var formData = {};
	  formData.oktaHost = AJS.$('#oktaHost').val();
	  formData.oktaApiToken = AJS.$('#oktaApiToken').val();
	  formData.applications = []; 
	  var apps = AJS.$('#oktaAppsList>div');
	  $.each( apps, function( index, fieldGrp ) {
		  var id = $(fieldGrp).attr("id");
		  var label = $(fieldGrp).children("input[name='label']").val();
		  var applicationLink = $(fieldGrp).children("input[name='applicationLink']").val();
		  var enabled = $(fieldGrp).children("input:checkbox[name=enabled]:checked").length > 0;  
		  var applicationLogo = $(fieldGrp).children("img").attr("src");
		  var oktaApplication = $(fieldGrp).children("input[name='oktaApplication']").val();
		  formData.applications.push({
			  "applicationId": id,
			  "label" : label,
			  "oktaApplication": oktaApplication == "true",
			  "enabled": enabled, 
			  "applicationLink": applicationLink,
			  "applicationLogo": applicationLogo
		  });
	  });

	  AJS.$.ajax({
	    url: AJS.contextPath()  + "/rest/oktaclient-config/1.0/oktaclient-config",
	    type: "PUT",
	    contentType: "application/json",
	    data: JSON.stringify(formData),
	    processData: false,
	    error: function(jqXHR, textStatus, errorThrown) {
	    	var error = JSON.parse(jqXHR.responseText);
	    	if (error.fieldName) {
	    		AJS.$("#errorMessage").html(error.errorCode);
	    		AJS.$("#errorMessage").css('display','');
	    	} else if (error.message) {
	    		
	    	}
	    },
	    success: function () {
	    	console.log("reload page");
	    	location.reload();
	    }
	  });

	}
  
  populateForm();
  
  AJS.$("#oktaClientAdmin").submit(function(e) {
	    e.preventDefault();
	    AJS.$(".error").css("display", "none");
	    updateConfig();
	});

  AJS.$(".applicationLogo").click(function(e){
      var me = $(this);
      var popup = new AJS.Dialog(500, 200);
      // create a dialog 860px wide x 530px high
      popup.addHeader("Application Logo", "aui");
      popup.addButton("Save", function (popup) {  
    	  var srcImg = $('#applicationLogoUrl').val();
    	  me.attr("src", srcImg);
    	  popup.remove();
      }, "#");
      popup.addButton("Cancel", function (popup) {
    		  popup.remove();
      }, "#");
      // add panel 1
      popup.addPanel("Logo Panel", 
    		  '<img id="applicationEditLogo" style="height: 2.1428571428571em;" src="'+ $(me).attr('src')+'"/> ->' +
    		  '<img id="applicationEditLogoNew" style="height: 2.1428571428571em;" src=""/>' +
    		  '<p><input type="text" id="applicationLogoUrl" name="applicationLogoUrl" class="text" value="'+$(me).attr('src')+'" maxlength="255" style="width: 450px;"/></p>'
    		  , "panel-body");
      // You can remove padding with:
      // dialog.get("panel:0").setPadding(0);
      
      $('#applicationLogoUrl').keyup(function(e){
    	  if (e.keyCode == 13) {
    		  $('#applicationEditLogoNew').attr("src", $('#applicationLogoUrl').val());
    	  }
      });
      
      popup.show();
      return false;
  });
  
  AJS.$("#addOktaApp").click(function(e) {
	    e.preventDefault();
	    var relativeUrl = AJS.Data.get("context-path");
	    AJS.$('#oktaAppsList').append(
	    		'<div class="field-group" id="">' +
	    		'  <input type="checkbox" id="enabled" name="enabled" checked="checked"></input>' +
	            '  <input type="hidden" id="oktaApplication" name="oktaApplication" value="false"/>'+
	    		'  <input type="text" id="label" name="label" class="text"/>' +
	    		'  <input type="text" id="applicationLink" name="applicationLink" class="text" value="" style="max-width: 450px;"/>'+
	            '  <img id="applicationLogo" class="applicationLogo" style="height: 2.1428571428571em; vertical-align: bottom;" src="' +
	            relativeUrl + '/download/resources/com.isostech.atlassian.confluence.okta-confluence-plugin:okta-confluence-plugin-resources/images/okta_23x23.png"/>'+
	    		'</div>');   
	});
  
  AJS.$("#flushApps").click(function(e) {
	  $.ajax({
          url: AJS.contextPath() + "/rest/user-application/1.0/user-application?command=flushCache",
          type: "POST",
          dataType: "json",
          contentType: "application/json",
          success: function (response) {
        	  // lets have some nice greed checkbox next to the button
          }
      }); 
  	});
  
});
