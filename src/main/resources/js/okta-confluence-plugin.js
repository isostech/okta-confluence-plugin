/*******************************************************************************
 * Copyright 2014 Isos Technology, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

AJS.toInit(function ($) {
    var dialogId = "okta-user-apps-dialog";
    var $webItem = $('#okta-user-apps');

    function  populateApplicationLinks() {
        $.ajax({
            url: AJS.contextPath() + "/rest/user-application/1.0/user-application",
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            error:function () {
                content.html(Confluence.Templates.Plugins.Okta.Client.error());
            },
            success: function (response) {
            	if (response.apps && response.apps.length > 0) {
	            	$webItem.html("<ul id='okta-user-apps-ul'></ul>");
	            	var userApps = $webItem.find('#okta-user-apps-ul')
	            	$.each(response.apps, function(idx,app) {
	            		userApps.append('<li><a href="' + app.applicationLink + '" target="_blank">' +
	                			'<img height="23px" src="' + app.applicationLogo + '" title="' + app.label + '" ' +
	                			'alt="' + app.label + '"/></a></li>');
	            	});
	            	$webItem.append('</ul>');
            	}
            }
        });    	
    };
    
    populateApplicationLinks();
});
